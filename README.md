## A23-SU335-8.1-IN-V033-20200725
- Manufacturer: itel mobile limited
- Platform: sp9832e
- Codename: itel-A23
- Brand: Itel
- Flavor: sp9832e_1h10_gofu_nofp-user
- Release Version: 8.1.0
- Kernel Version: 4.4.83
- Id: OPM2.171019.012
- Incremental: IN-V033-20200725
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Itel/SU335/itel-A23:8.1.0/OPM2.171019.012/IN-V033-20200725:user/release-keys
- OTA version: 
- Branch: A23-SU335-8.1-IN-V033-20200725
- Repo: itel_itel-a23_dump
