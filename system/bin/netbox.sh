#!/system/bin/sh

LOG_TAG="netbox.sh"

function nlog()
{
	log -p d -t "$LOG_TAG" $@ 
}

function exe_log()
{
	local cmd="$@"
	local emsg="$(eval $cmd 2>&1)"

	log -p d -t "$LOG_TAG" "$cmd ecode=$? $emsg"
}

function gro_switch()
{
	exe_log "echo $1 > /sys/module/seth/parameters/gro_enable"
}

#for reliance to have BIP test over ipv6
function stk_bip_test()
{
	nlog "start stk logic"
	is_stk=`getprop persist.sys.bip.channel`
	nlog "is_stk = $is_stk."
	if [ $is_stk == "1" ]; then
		nlog "now it is bip test over ipv6"
		ip_addr=`getprop persist.sys.bip.ipv6_addr`
		table_idx=`getprop persist.sys.bip.table_index`
		nlog "ip addr = $ip_addr."
		nlog "table index = $table_idx."
		exe_log "ip -6 rule add from $ip_addr lookup $table_idx"
	else
		nlog "is_stk is not set!"
	fi
}

if [ "$1" == "gro" ]; then
	if [ "$2" == "on" ]; then
		gro_switch 1;
	elif [ "$2" == "off" ]; then
		gro_switch 0;
	fi
elif [ "$1" == "stk" ]; then
	stk_bip_test    
fi
