#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery:36700160:a0ba68adba00bd92c2e1e66a6675fdb614e2d159; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/boot:36700160:67d09aff3b8576b231fa127503598c92c21c1e78 EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery a0ba68adba00bd92c2e1e66a6675fdb614e2d159 36700160 67d09aff3b8576b231fa127503598c92c21c1e78:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
